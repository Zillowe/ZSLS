/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./src/**/*.{html,js,svelte,ts}",
    "./node_modules/flowbite-svelte/**/*.{html,js,svelte,ts}",
  ],
  plugins: [require("daisyui"), require('flowbite/plugin')],
  darkMode: 'class',
  daisyui: {
    themes: ["dracula"],
  },
  theme: {
    extend: {
      
    },
  },
}